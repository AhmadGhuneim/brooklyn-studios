<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToDoMirror extends Model
{
    protected $table = 'to_do_mirrors';
    protected $fillable = ['id','name','description','created_at','updated_at'];
}
