<form action="{{route('toDoTask.update',[$toDoTask->id])}}" method="post">
    {{csrf_field()}}
    {{method_field('PUT')}}
    <div class="form-group row">
        <div class="col-8">
            <p class="text-left addTaskTitle">
                Update Task
            </p>
        </div>
        <div class="col-4">
            <a href="{{route('toDoTask.index')}}" class="btn btn-success">
                Create new task
            </a>
        </div>
    </div>
    <div class="form-group row">
        <label for="name"
               class="col-sm-12">Name</label>
        <div class="col-sm-12">
            <input type="text"
                   class="form-control"
                   value="{{!empty(old('name'))?old('name'):$toDoTask->name}}"
                   id="name" name="name" placeholder="name">
            @error('name')
            <div class="error_form text-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="description"
               class="col-sm-12">Description</label>
        <div class="col-sm-12">
                        <textarea
                                class="form-control"
                                id="description"
                                name="description"
                                placeholder="description">{{!empty(old('description'))?old('description'):$toDoTask->description}}</textarea>
            @error('description')
            <div class="error_form text-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-12 text-center">
            <input type="submit" class="btn btn-primary submit-btn" value="Update">
        </div>
    </div>
</form>
