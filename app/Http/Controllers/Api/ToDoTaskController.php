<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ToDoTaskRequest;
use App\Http\Resources\ToDoTask as ToDoTaskResource;

use App\ToDoMirror;
use App\ToDoTask;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;

class ToDoTaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(
            ['toDoTasks'=>ToDoTask::all()]
        );
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ToDoTaskRequest $request)
    {
        $toDoTask = ToDoTask::create($request->all());
        return response()->json([
            'message'=>'To Do task created successfully',
            'data'=>$toDoTask
        ],201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ToDoTask  $toDoTask
     * @return \Illuminate\Http\Response
     */
    public function show(ToDoTask $toDoTask)
    {
        return response()->json([
            'message'=>'To Do task updated successfully',
            'data'=>$toDoTask
        ],200);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ToDoTask  $toDoTask
     * @return \Illuminate\Http\Response
     */
    public function update(ToDoTaskRequest $request, ToDoTask $toDoTask)
    {
        $toDoTask->update($request->all());
        return response()->json([
            'message'=>'To Do task updated successfully',
            'data'=>$toDoTask
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ToDoTask  $toDoTask
     * @return \Illuminate\Http\Response
     */
    public function destroy(ToDoTask $toDoTask)
    {
        $toDoTask->delete();
        return response()->json([
            'message'=>'Task deleted successfully',
        ],200);
    }
}
