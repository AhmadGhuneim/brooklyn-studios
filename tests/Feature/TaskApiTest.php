<?php

namespace Tests\Feature;

use App\ToDoTask;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TaskApiTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testCanCreateTask()
    {
        $formData = [
            'name'=>'Testing tasks',
            'description'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,.',
        ];
        $this->withoutExceptionHandling();
        $this->json('POST',route('toDoTaskApi.store'),$formData)
        ->assertStatus(201)
        ->assertJson(['data'=>$formData]);
    }
    public function testCanShowTasks(){
        $this->get(route('toDoTaskApi.index'))
            ->assertStatus(200);
    }
    public function testCanUpdateTasks(){
        $formData = [
            'name'=>'Testing tasks',
            'description'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s,.',
        ];
        $task = factory(ToDoTask::class)->create();
        $this->withoutExceptionHandling();
        $this->json('PUT',
            route('toDoTaskApi.update',[$task->id]),
            $formData)
            ->assertStatus(200)
        ;
    }
    public function testCanGetOneTask(){
        $task = factory(ToDoTask::class)->create();
        $this->withoutExceptionHandling();
        $this->get(
            route('toDoTaskApi.show',[$task->id]))
            ->assertStatus(200);
    }
    public function testCanDeleteTask(){
        $task = factory(ToDoTask::class)->create();
        $this->withoutExceptionHandling();
        $this->delete(
            route('toDoTaskApi.destroy',[$task->id]))
            ->assertStatus(200);
    }
}
