<form action="{{route('toDoTask.store')}}" method="post">
    {{csrf_field()}}
    <div class="form-group row">
        <div class="col-12">
            <p class="text-left addTaskTitle">
                Add A Task
            </p>
        </div>
    </div>
    <div class="form-group row">
        <label for="name"
               class="col-sm-12">Name</label>
        <div class="col-sm-12">
            <input type="text"
                   class="form-control"
                   value="{{old('name')}}"
                   id="name" name="name" placeholder="name">
            @error('name')
            <div class="error_form text-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="description"
               class="col-sm-12">Description</label>
        <div class="col-sm-12">
                        <textarea
                                class="form-control"
                                id="description"
                                name="description"
                                placeholder="description">{{old('description')}}</textarea>
            @error('description')
            <div class="error_form text-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-12 text-center">
            <input type="submit" class="btn btn-primary submit-btn" value="Add">
        </div>
    </div>
</form>
