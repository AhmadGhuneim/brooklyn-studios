@if(!empty($toDoTasks))
<div class="row padding-30">
    <div class="col-12 previous-tasks-header">
        <p>
        To Do Tasks
        </p>
    </div>

    <div class="col-12 background-white to-do-tasks-lists ">
        @foreach($toDoTasks as $toDoTask)
            <div class="border-bottom-1 row">
            <div class="col-6 text-left">
                #{{$toDoTask->id}}
            </div>
                <div class="col-6 text-right">
                    <a href="{{route('toDoTask.destroy',[$toDoTask->id])}}"
                       onclick="event.preventDefault();
                                                     document.getElementById('destroy-form').submit();"
                    >
                    <i class="fa fa-trash text-danger" aria-hidden="true"></i>
                        <form id="destroy-form" action="{{ route('toDoTask.destroy',[$toDoTask->id]) }}"
                              method="POST" style="display: none;">
                            {{method_field('delete')}}
                            @csrf
                        </form>
                    </a>
                    <a href="{{route('toDoTask.edit',[$toDoTask->id])}}">
                    <i class="fas fa-pencil-alt text-primary" aria-hidden="true"></i>
                    </a>
                </div>
            <div class="col-12 text-left font-size-20">
                {{$toDoTask->name}}
            </div>
            <div class="col-12 text-left">
                {{$toDoTask->description}}
            </div>
            </div>
        @endforeach
    </div>
</div>
@endif
