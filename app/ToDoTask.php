<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToDoTask extends Model
{
    protected $table = 'to_do_tasks';
    protected $fillable = ['id','name','description','created_at','updated_at'];

    public static function boot() {
        parent::boot();
        static::deleting(function($toDoTask) { // before delete() method call this
            ToDoMirror::find($toDoTask->id)
                ->delete();
        });
        self::created(function($toDoTask){
            ToDoMirror::create($toDoTask->toArray());
        });
        self::updated(function($toDoTask){
            $toDoMirror = ToDoMirror::find($toDoTask->id);
            $toDoMirror->update($toDoTask->toArray());
        });
    }
}
