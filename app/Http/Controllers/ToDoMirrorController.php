<?php

namespace App\Http\Controllers;

use App\ToDoMirror;
use Illuminate\Http\Request;

class ToDoMirrorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ToDoMirror  $toDoMirror
     * @return \Illuminate\Http\Response
     */
    public function show(ToDoMirror $toDoMirror)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ToDoMirror  $toDoMirror
     * @return \Illuminate\Http\Response
     */
    public function edit(ToDoMirror $toDoMirror)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ToDoMirror  $toDoMirror
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ToDoMirror $toDoMirror)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ToDoMirror  $toDoMirror
     * @return \Illuminate\Http\Response
     */
    public function destroy(ToDoMirror $toDoMirror)
    {
        //
    }
}
