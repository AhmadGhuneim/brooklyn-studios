<?php

namespace App\Http\Controllers;

use App\Http\Requests\ToDoTaskRequest;
use App\Http\Resources\ToDoTask as ToDoTaskResource;

use App\ToDoTask;
use Illuminate\Support\Facades\Session;

class ToDoTaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('toDoTask.index')
            ->with('toDoTasks',ToDoTask::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('toDoTask.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ToDoTaskRequest $request)
    {
        ToDoTask::create($request->all());
        Session::flash('success','ToDo task created successfully');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ToDoTask  $toDoTask
     * @return \Illuminate\Http\Response
     */
    public function show(ToDoTask $toDoTask)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ToDoTask  $toDoTask
     * @return \Illuminate\Http\Response
     */
    public function edit(ToDoTask $toDoTask)
    {
        return view('toDoTask.index')
            ->with('toDoTask',$toDoTask)
            ->with('toDoTasks',ToDoTask::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ToDoTask  $toDoTask
     * @return \Illuminate\Http\Response
     */
    public function update(ToDoTaskRequest $request, ToDoTask $toDoTask)
    {
        $toDoTask->update($request->all());
        Session::flash('info','ToDo task created successfully');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ToDoTask  $toDoTask
     * @return \Illuminate\Http\Response
     */
    public function destroy(ToDoTask $toDoTask)
    {
        $toDoTask->delete();
        Session::flash('error','ToDo task deleted successfully');
        return redirect()->back();
    }
}
