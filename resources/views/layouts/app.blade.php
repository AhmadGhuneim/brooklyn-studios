<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/site.css') }}" rel="stylesheet">

</head>
<body>
    <div id="app" class="height-100">
        <div class="container-fluid height-100">
            <div class="row height-100">
            <div class="col-2 height-100 d-none d-sm-none d-md-table" id="aside-bar">
                <p class="main-title">
                    To Do Task Application
                </p>
            </div>
                <div class="col-10 main-content">
                    @if(Session::has('success'))
                        <div class="alert alert-success" style="text-align: center;margin-top: 30px;">
                            {{Session::get('success')}}
                        </div>
                    @endif
                    @if(Session::has('error'))
                        <div class="alert alert-danger" style="text-align: center;margin-top: 30px;">
                            {{Session::get('error')}}
                        </div>
                    @endif
                    @if(Session::has('info'))
                        <div class="alert alert-info" style="text-align: center;margin-top: 30px;">
                            {{Session::get('info')}}
                        </div>
                    @endif
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

</body>
</html>
