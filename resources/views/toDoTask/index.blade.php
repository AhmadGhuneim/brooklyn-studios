@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-6">
            @if(isset($toDoTask))
                @include('toDoTask.formUpdate')
            @else
                @include('toDoTask.form')
            @endif
        </div>
        <div class="col-md-6">
            @include('toDoTask.previousTasks')
        </div>
    </div>
@endsection
